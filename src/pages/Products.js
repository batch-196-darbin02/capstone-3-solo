import {useState, useEffect} from 'react';
// import productsData from '../data/productsData';
import ProductCard from '../components/ProductCard';
export default function	Products(){


	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://mysterious-bayou-08419.herokuapp.com/products/viewAllActive')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product}/>
				)
			}))
		})

	}, [])


	return(
		<>
			<h1>Available Products</h1>
			{products}
		</>
	);
}
