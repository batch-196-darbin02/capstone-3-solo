import {useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, Link} from 'react-router-dom';
// import productsData from '../data/productsData';
import ProductCard from '../components/ProductCard';
export default function	Profile(){

	const [user, setUser] = useState([]);
	const [isActive, setIsActive] = useState(false);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const {userId} = useParams();

	useEffect(() => {
		fetch('https://mysterious-bayou-08419.herokuapp.com/users/userDetails',{
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: userId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setMobileNo(data.mobileNo);

		});

	}, [userId])


	return(
			<>
			<h1>Profile </h1>
			<Container className='mt-5'>
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Subtitle>Firstname:</Card.Subtitle>
							<Card.Text>{firstName}</Card.Text>
							<Card.Subtitle>Lastname:</Card.Subtitle>
							<Card.Text>{lastName}</Card.Text>
							<Card.Subtitle>Email:</Card.Subtitle>
							<Card.Text>{email}</Card.Text>
							<Card.Subtitle>Mobile Number:</Card.Subtitle>
							<Card.Text>{mobileNo}</Card.Text>
							<Link className='btn btn-primary' to='/'>Home</Link>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</>

	);
}
