import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	// Allows us to consume the User context object and it's properties to use for user validation
	// useContext(Provider);
	const {user, setUser} = useContext(UserContext);
		console.log(user);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	function authenticate(e){

		e.preventDefault();

		// fetch('URL',options)
		fetch('https://mysterious-bayou-08419.herokuapp.com/users/login',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to SkyAir Online Store'
				});

			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check your credentials'
				});
			};
		});

		setEmail('');
		setPassword('');

	};

	const retrieveUserDetails = (token) => {

		fetch('https://mysterious-bayou-08419.herokuapp.com/users/userDetails',{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	};

	useEffect (() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		};

	}, [email,password])

	return(
		(user.id !== null) ?
			<Navigate to='/products'/>
		:
		<>
		<h1>Login</h1>
		<Form onSubmit={e => authenticate(e)}>
			<Form.Group controlId='userEmail'>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type='email' placeholder='Enter your email here' required value={email}
					onChange={e => setEmail(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='password'>
				<Form.Label>Password</Form.Label>
				<Form.Control type='password' placeholder='Enter your password here' required 
				value={password} onChange={e => setPassword(e.target.value)} />
			</Form.Group>

			<p>Not yet registered? Create an account <Link to='/register' style={{textDecoration:'none'}}>here.</Link></p>

			{ isActive ?
				<Button className="mt-3 mb-5" variant='success' type='submit' id='submitBtn'>
				Login
				</Button>
				:
				<Button className="mt-3 mb-5" variant='danger' type='submit' id='submitBtn' disabled>
				Login
				</Button>
			}
		</Form>
		</>
	)

}