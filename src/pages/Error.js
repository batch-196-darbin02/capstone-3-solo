import {Link} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';

export default function Error(){
	return(
		<Row>
			<Col className='p-5'>
				<h1>Error 404</h1>
				<h3>Sorry, we could not find that page.</h3>
				<p className="text-muted">Please go back to home page.</p>
				<Button clssName="mt-3 mb-5" variant='primary' type='submit' as={Link} to='/'>
				Back Home
				</Button>
			</Col>
		</Row>

	)
}