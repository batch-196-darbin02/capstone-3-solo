import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const [isActive, setIsActive] = useState(false);

	function registerUser(e){

		e.preventDefault();

		fetch(`https://mysterious-bayou-08419.herokuapp.com/users/checkEmailExists`,{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) 

			if(data){
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'info',
					text: `The email that you're trying to register already exist.`
				})
			} else {
				fetch('https://mysterious-bayou-08419.herokuapp.com/users',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data.email){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Thank you for registering'
						});
						history('/login');
					} else {
						Swal.fire({
							title: 'Registration failed.',
							icon: 'error',
							text: 'Something went wrong, try again'
						})
					}
				})
			}
		})

		setEmail('');
		setPassword('')
		setFirstName('');
		setLastName('');
		setMobileNo('');
	}


	useEffect (() => {
		if(email !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length === 11 && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		};

	}, [email,firstName,lastName,mobileNo,password])
	return(
		(user.id !== null) ?
			<Navigate to='/products'/>
		:
		<>
		<Container>
		<h1>Create an Account</h1>
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group controlId='firstName' className='mt-3'>
				<Form.Label style={{fontWeight:'bold'}}>Name</Form.Label>
				<Form.Control
					type='text' placeholder='Enter your first name here' required value={firstName}
					onChange={e => setFirstName(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='lastName' className='mt-2'>
				<Form.Control
					type='text' placeholder='Enter your last name here' required value={lastName}
					onChange={e => setLastName(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='mobileNo' className='mt-3'>
				<Form.Label style={{fontWeight:'bold'}}>Mobile Number</Form.Label>
				<Form.Control
					type='number' placeholder='Enter your 11-digit mobile number here' required value={mobileNo}
					onChange={e => setMobileNo(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='userEmail' className='mt-3'>
				<Form.Label style={{fontWeight:'bold'}}>Email Address</Form.Label>
				<Form.Control
					type='email' placeholder='Enter your email here' required value={email}
					onChange={e => setEmail(e.target.value)} />
				<Form.Text className='text-muted'>
					We'll never share your email address with anyone else, trust us!
				</Form.Text>
			</Form.Group>

			<Form.Group controlId='password' className='mt-3'>
				<Form.Label style={{fontWeight:'bold'}}>Password</Form.Label>
				<Form.Control type='password' placeholder='Enter your password here' required 
				value={password} onChange={e => setPassword(e.target.value)} />
			</Form.Group>


			{ isActive ?
				<Button className="mt-4 mb-5" variant='success' type='submit' id='submitBtn'>
				Register
				</Button>
				:
				<Button className="mt-4 mb-5" variant='danger' type='submit' id='submitBtn' disabled>
				Register
				</Button>
			}	
		</Form>
		</Container>
		</>
	)
}