import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){
	return(
		<Row>
			<Col className='p-5'>
				<h1>E-Commerce App</h1>
				<p>Awesome Products  |  Super Deals  |  Delivering Comfort</p>
				<Link className='btn btn-primary' to='/login'>Login to Order</Link>
			</Col>
		</Row>

	)
}