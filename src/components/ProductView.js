import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const {user} = useContext(UserContext);

	// allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const history = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const {productId} = useParams();

	const order = (productId) => {
		fetch(`https://mysterious-bayou-08419.herokuapp.com/orders/createOrder`,{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: 'Sucessfully ordered!',
					icon: 'success',
					text: 'Thank you for ordering.'
				});

				history('/products');

			} else {
				Swal.fire({
					title: 'Something when wrong.',
					icon: 'error',
					text: 'Please try again later.'
				});
			};
		});
	};

	useEffect(() => {
		console.log(productId)
		fetch(`https://mysterious-bayou-08419.herokuapp.com/products/viewOne/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [productId]);

	return (

		<Container className='mt-5'>
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{user.id !== null ?
								<Button variant='primary'onClick={() => order(productId)}>Order</Button>
								:
								<Link className='btn btn-danger' to='/login'>Login to order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}