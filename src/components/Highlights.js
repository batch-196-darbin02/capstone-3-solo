import React, { useState } from 'react';
import home from '../images/home.jpg';
import home02 from '../images/home02.jpg';
import commercial from '../images/commercial.jpg';
import office from '../images/office.jpg';
import Carousel from 'react-bootstrap/Carousel';

export default function Highlights(){
	const [index, setIndex] = useState(0);

	const handleSelect = (selectedIndex, e) => {
    	setIndex(selectedIndex);
    };

	return (
	    <Carousel activeIndex={index} onSelect={handleSelect} className='mt-3 mb-3'>
	    	<Carousel.Item>
	    		<img
		        	className="d-block w-100 h-100px"
		        	src={home}
		        	alt="First slide"
		        />
				<Carousel.Caption style={{backgroundColor: 'rgba(0,0,0,0.5)'}}>
					<h3>SkyAir No.1</h3>
					<p>SkyAir Airconditioning offer a wide range of airconditioning solutions to purify and cool indoor spaces for comfort with energy efficiency for a lesser environment impact.</p>
				</Carousel.Caption>
			</Carousel.Item>

			<Carousel.Item>
				<img
					className="d-block w-100"
					src={home02}
					alt="Second slide"
				/>
				<Carousel.Caption style={{backgroundColor: 'rgba(0,0,0,0.5)'}}>
					<h4>Perfecting the Air</h4>
					<p>QUIET & COMFORT FLOW</p>
				</Carousel.Caption>
			</Carousel.Item>

			<Carousel.Item>
				<img
					className="d-block w-100"
					src={commercial}
					alt="Third slide"
				/>
				<Carousel.Caption style={{backgroundColor: 'rgba(0,0,0,0.5)'}}>
					<h3>Care Everywhere You Stay</h3>
					<p>SkyAir leads the light commercial market with solutions that offer design flexibility corresponding to any commercial setting.</p>
				</Carousel.Caption>
			</Carousel.Item>

			<Carousel.Item>
				<img
					className="d-block w-100"
					src={office}
					alt="Fourth slide"
				/>
				<Carousel.Caption style={{backgroundColor: 'rgba(0,0,0,0.5)'}}>
					<h3>Multi-Split Airconditioner</h3>
					<p>Multi-split type air conditioner for commercial buildings that uses variable refrigerant flow control developed to provide customers with the ability to maintain individual zone control in each room and floor of a building.</p>
				</Carousel.Caption>
			</Carousel.Item>

	    </Carousel>
	);

}